import { Component, OnInit } from '@angular/core';
import { BookService, SearchResult } from '../book.service';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent implements OnInit {
query:string='';
results?:SearchResult;
  constructor(private bS:BookService) { }

  ngOnInit(): void {
  }
onSubmit(){
  this.bS.search(this.query).subscribe(data => this.results=data);
  console.log(this.results);
}
}
