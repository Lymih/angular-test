import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookComponent } from './book.component';

describe('BookComponent', () => {
  let component: BookComponent;
  let fixture: ComponentFixture<BookComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should assignate book', () => {
    expect(component).toBeTruthy();
    component.book = {title:'test',first_publish_year:'1920',author_name:["Tolkien"],cover_i:123,subject:['blabla']}
    fixture.detectChanges()
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.innerHTML).toContain('Tolkien');
  });

  it('should render Unknown', () => {
    component.book ={title:'test',first_publish_year:'1920',cover_i:123,subject:['blabla']}
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
      expect(compiled.innerHTML).toContain('Unknown');
    });
  
    it('should render empty if no subject', () => {
      component.book ={title:'test',first_publish_year:'1920',cover_i:123}
      fixture.detectChanges();
      const compiled = fixture.nativeElement as HTMLElement;
        expect(compiled.innerHTML).not.toContain('Subject');
      });
      
      it('should render empty if no subject', () => {
        component.book ={title:'test',first_publish_year:'1920',cover_i:123}
        fixture.detectChanges();
        const compiled = fixture.nativeElement as HTMLElement;
          expect(compiled.innerHTML).not.toContain('Subject');
        });
});
