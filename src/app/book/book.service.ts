import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { count, map } from 'rxjs';
import { Book } from '../entities';



export interface SearchResult {
  numFound:number;
  docs:Book[];
}

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http:HttpClient) { }

 search(query:string){
  return this.http.get<SearchResult>("https://openlibrary.org/search.json?q="+query);
  
  }
}
