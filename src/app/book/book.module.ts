import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from "@angular/common/http";
import { SearchPageComponent } from './search-page/search-page.component';
import { BookRoutingModule } from './book-routing.module';
import { FormsModule } from '@angular/forms';
import { BookComponent } from './book/book.component';



@NgModule({
  declarations: [
    SearchPageComponent,
    BookComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    BookRoutingModule,
    FormsModule
    
  ]
})
export class BookModule { }
