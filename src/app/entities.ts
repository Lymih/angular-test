export interface Book {
    title:string;
    first_publish_year:string;
    author_name?:string[];
    cover_i?:number;
    subject?:string[];
}