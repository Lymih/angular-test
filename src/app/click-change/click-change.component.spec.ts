import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClickChangeComponent } from './click-change.component';

describe('ClickChangeComponent', () => {
  let component: ClickChangeComponent;
  let fixture: ComponentFixture<ClickChangeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClickChangeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ClickChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render message', () => {
    const fixture = TestBed.createComponent(ClickChangeComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('p')?.textContent).toContain('Coucou');
  });
  it('should change message', () => {
  component.action();
  fixture.detectChanges();
  const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.innerHTML).toContain('Autre chose');
  });
});
